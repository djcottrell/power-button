# power-button

## Installation / Uninstall

1. Press Start
1. Go to Quit
1. Select Quit Emulation Station
1. Type the following `git clone git@bitbucket.org:djcottrell/power-button.git`
1. Run the setup script by typing in `./power-button/scripts/install`
1. For uninstall you can use `./pi-power-button/scripts/uninstall`

